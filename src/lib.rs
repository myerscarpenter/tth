#![deny(unsafe_code)]

use digest::generic_array::typenum::U24;
use digest::generic_array::GenericArray;
use digest::Digest;
use std::convert::TryInto;
use tiger::Tiger;

const BLOCK_SIZE: usize = 1024;

#[derive(Clone)]
pub struct TTH {
    tree: Vec<Vec<[u8; 24]>>,
    byte_counter: usize,
    tiger_context: Tiger,
}

impl Default for TTH {
    fn default() -> Self {
        TTH {
            tree: vec![vec![]],
            byte_counter: 0,
            tiger_context: Default::default(),
        }
    }
}

impl digest::Reset for TTH {
    fn reset(&mut self) {
        self.tree = vec![vec![]];
        self.byte_counter = 0;
        self.tiger_context = Default::default();
    }
}

impl TTH {
    #[inline]
    fn update_pick(&mut self, data: impl AsRef<[u8]>) {
        let buffer: &[u8] = data.as_ref();
        if self.byte_counter + buffer.len() > BLOCK_SIZE {
            self.update_end_of_leaf(buffer);
        } else {
            self.update_leaf(buffer);
        }
    }

    #[inline]
    fn update_leaf(&mut self, data: impl AsRef<[u8]>) {
        let buffer: &[u8] = data.as_ref();

        if self.byte_counter == 0 {
            digest::Digest::update(&mut self.tiger_context, b"\0");
        }
        digest::Update::update(&mut self.tiger_context, buffer);

        self.byte_counter += buffer.len();
    }

    #[inline]
    fn update_end_of_leaf(&mut self, data: impl AsRef<[u8]>) {
        let buffer: &[u8] = data.as_ref();

        let end = BLOCK_SIZE - self.byte_counter;
        let (left, buffer): (&[u8], &[u8]) = buffer.split_at(end);
        self.update_leaf(left);
        let something = self.tiger_context.finalize_reset();
        let x: [u8; 24] = something.try_into().unwrap();
        self.tree[0].push(x);
        self.byte_counter = 0;
        self.update_pick(buffer);
    }

    // lower level could have 1 or more hash in it
    // output should have lower_level.len() / 2 members
    #[inline]
    fn compute_level(&self, lower_level: &[[u8; 24]]) -> Vec<[u8; 24]> {
        let mut level = vec![];
        let mut context = Tiger::new();

        let count = (lower_level.len() / 2) * 2;
        for (idx, leaf) in lower_level.iter().take(count).enumerate() {
            if idx % 2 == 0 {
                digest::Update::update(&mut context, b"\x01");
            }
            digest::Update::update(&mut context, leaf);
            if (idx + 1) % 2 == 0 {
                let something = context.finalize_reset();
                let x: [u8; 24] = something.try_into().unwrap();
                level.push(x);
            }
        }
        if lower_level.len() % 2 == 1 {
            level.push(*lower_level.last().unwrap());
        }
        level
    }

    fn finalize(&mut self) {
        let something = self.tiger_context.finalize_reset();
        let x: [u8; 24] = something.try_into().unwrap();
        self.tree[0].push(x);

        let mut counter = 0;

        while self.tree[counter].len() > 1 {
            self.tree.push(self.compute_level(&self.tree[counter]));

            counter += 1;
        }
    }
}
impl digest::Update for TTH {
    #[inline]
    fn update(&mut self, data: impl AsRef<[u8]>) {
        self.update_pick(data.as_ref());
    }
}

impl digest::FixedOutputDirty for TTH {
    type OutputSize = U24;

    fn finalize_into_dirty(&mut self, out: &mut GenericArray<u8, Self::OutputSize>) {
        self.finalize();
        out.copy_from_slice(self.tree.last().unwrap().first().unwrap());
    }
}

#[cfg(test)]
mod test {
    use super::TTH;
    use digest::Digest;
    use hex_literal::{hex, hex_impl};

    #[test]
    fn test_vectors() {
        let examples = [
            // test vectors from http://web.archive.org/web/20080316033726/http://www.open-content.net/specs/draft-jchapweske-thex-02.html#anchor17
            ("", hex!("5d9ed00a030e638bdb753a6a24fb900e5a63b8e73e6c25b6")),
            (
                "\0",
                hex!("aabbcca084acecd0511d1f6232a17bfaefa441b2982e5548"),
            ),
            (
                &"A".repeat(1024),
                hex!("5fbd0e62ad016d596b77d1d28883b94fed78ecbaf4640914"),
            ),
            (
                &"A".repeat(1025),
                hex!("7e591c1cd8f2e6121fdbcd8071ba279626b771642d10a3db"),
            ),
            // extra test vectors
            (
                &"a".repeat(10240),
                hex!("5dac1bf713118819035996d51385a99abe875776f54d35a5"),
            ),
            (
                &"a".repeat(10241),
                hex!("170c4d753730b9a37d088db0d754ed7e1823d1d9d9124f80"),
            ),
        ];

        for &(input, answer) in examples.iter() {
            let result = TTH::digest(input.as_bytes());
            assert_eq!(answer, *result);
        }
    }
}
